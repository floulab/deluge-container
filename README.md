# HOWTO

### Run container:

```sh
$ docker run -d -p 55100:55100 -p 55100:55100/udp -p 8112:8112 \
             --name deluge \
             -v /host/deluge/home_dir:/var/lib/deluge \
             -e TZ="Europe/Athens" \
             floulab/deluge
```

### Details

Change perms in /host/deluge/home_dir to the deluge user. You may need to create the user in the host.

Go to preferences in web-ui and set the torrent port 55100, set the directories for downloads etc.

You can pass your prefered timezone with the TZ ENV Variable.

