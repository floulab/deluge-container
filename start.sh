#!/bin/bash

getent group $DELUGE_GROUP
group_exists=$?

getent passwd $DELUGE_USER
user_exists=$?

if [ $group_exists -ne 0 ]; then
  groupadd -g $DELUGE_GROUP_UID $DELUGE_GROUP
fi

if [ $user_exists -ne 0 ]; then
  useradd -d $DELUGE_HOME -s /bin/false -u $DELUGE_USER_UID -g $DELUGE_GROUP_UID $DELUGE_USER
  chown -R $DELUGE_USER:$DELUGE_GROUP $DELUGE_HOME
fi

if [ ! -h /etc/localtime ]; then
    ln -sf /usr/share/zoneinfo/${TZ:-UTC} /etc/localtime && dpkg-reconfigure -f noninteractive tzdata
fi

if [ ! -d /supervisor_data ]; then
    mkdir -p /supervisor_data
    chown -R $DELUGE_USER:$DELUGE_GROUP /supervisor_data
fi

exec /usr/bin/supervisord -c /supervisord.conf
